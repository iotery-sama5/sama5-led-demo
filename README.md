# SAMA5 LED Demo

Code and Instructions for Running the Iotery SAMA5 LED demo firmware.

## Getting the Board

You can purchase the Microchip SAMA5 development board [ATSAMA5D27-SOM1-EK1 here](https://www.mouser.com/ProductDetail/Microchip-Technology/ATSAMA5D27-SOM1-EK1?qs=sGAEpiMZZMtw0nEwywcFgJjuZv55GFNmygJA6RLE2YKXN9MHE%2FoLuA%3D%3). The SAMA5 development board comes with wired communications such as ethernet, CAN, UARTs, and a few others. If you are looking for WiFi or BLE, hold tight, one is on the way from [SMT](https://teamsmt.com)!

## Flashing the SAMA5

You can download a basic Linux image pre-loaded with the firmware in this repository from here [here](https://drive.google.com/open?id=11KUYsTFBBkSh9s3qK6p8Gxt4Xz6bUrb8). This fully-installed image is just under 4GB in size once it is unzipped. It will easily flash and run on any standard SD card 4GB or larger. However, if you you use a larger SD card you will need to expand the main partition in order to have access to the additional space.

<!--
The ATSAMA5D27-SOM1-EK1 comes with a 4GB SD card - we suggest either using that, or a slightly bigger card (8GB).
You can find download the base image with the firmware in this repository preloaded in the [here](https://iotery-sama5.s3.us-east-2.amazonaws.com/iotery_led_demo_docker_v1.img.zip).
-->

### Flashing the SD Card (Linux/MacOS/Windows)

The easiest (and safest) way to flash the Linux image onto an SD card is to use the [Etcher](https://www.balena.io/etcher/) flashing tool. This tool is available for all major operating systems. Simply download Etcher and the [LED Demo Image](https://drive.google.com/open?id=11KUYsTFBBkSh9s3qK6p8Gxt4Xz6bUrb8), unzip the image archive `iotery_led_demo_docker_v1.1.img.zip`, and use Etcher to flash the image `iotery_led_demo_docker_v1.1.img` to the SD card.

You can also use the Linux `dd` command if you are more familiar with that method.

<!--
Flashing the provided SD card is made easy with [Etcher](https://www.balena.io/etcher/). Simply download Etcher, the [LED Demo Image](https://iotery-sama5.s3.us-east-2.amazonaws.com/iotery_led_demo_docker_v1.img.zip), unzip the file `iotery_led_demo_docker_v1.img.zip`, and flash the SD card with the downloaded image (`iotery_led_demo_docker_v1.img`).
-->

## Running the Demo

Once the SD card has been flashed, insert it securely into the SD card slot of the SAMA5 development board. Connect the board to an internet-connected network using a standard network cable and the on-board Ethernet socket. And finally, power the board using the micro USB socket located next to the Ethernet socket. It should be labeled `JLINK` or `JIO`. The micro USB socket can be connected to a standard 5V 1A+ power adapter, or to a powered USB port on a desktop or laptop. If connected to a desktop or laptop, the boards JLINK feature provides a TTY port over USB.

When the board is powered, it will begin the boot sequence. The indicator LED located next to the SD card slot will turn green for several seconds and then flash blue as Linux boots and goes through setup. On the first boot this can take a minute as new security keys are being generated. When the demo firmware starts, it will turn the LED off. After 10 to 15 seconds, the firmware will start the onboard webserver. At this point, you will need to connect to the webserver with your desktop browser via the local network in order to add your Iotery credentials. 

> Assuming the SAMA5 is connected to a standard router configured with DHCP, use the router's configuration page to determine the IP address to the SAMA5.

Once you determine the IP address of the SAMA5 (such as `192.168.1.100`), connect using any computer on the same local network by putting the IP address into a web browser. This should take you to the SAMA5 login page where you can enter the credentials:

- username: `sama5`
- password: `sama5`

At this point you should see three large buttons. If not, click the **Start** link in the top right corner. Assuming you have not configured your [Iotery](https://iotery.io) account to work with the demo, press the **Demo Setup** button. Follow the direction on the screen to walk through creating SAMA5 device and necessary types on your account. Once the setup is complete, click the **Click Here After Setup** button. This will take you to the device setup page and should fill in all the required data.

Once the device setup data is correct, the demo firmware will connect to Iotery and begin sending data. The control process attempts re-connects every 60 seconds, so there may be a delay after the device setup is compete. You can check the process by viewing the device log page, and you you can check connections states by viewing the device status page. Whenever the SAMA5 exchanges data with Iotery (about every 2 seconds), the green LED will turn on and off. A red LED indicates an error. Check the device log page.

You are now ready to test the connection by issuing a command issued from Iotery! On your [Iotery](https://iotery.io) SAMA5 Commands page, click the **Post Command** button for `TURN_ON_SAMA5_LED_DEMO`. This should cause the blue LED to turn on. Click the **Post Command** button for `TURN_OFF_SAMA5_LED_DEMO` to turn it off.

You can also watch a live feed of data coming from your SAMA5 on the [Iotery](https://iotery.io) SAMA5 Information page.

<!--

Once the SAMA5 is connected to Iotery ()

Once the SD card has been flashed, power up the ATSAMA5D27-SOM1-EK1 (SAMA5) with any standard 5V USB adaptor and attach it via ethernet to an internet-connected network. Once powered, the SAMA5 should be blinking ?. You may have to wait about 30 seconds for the board to boot up the webserver you will visit next.

> Assuming you plugged it into a standard router configured with DHCP, determine the IP address of the SAMA5 by visiting your router's configuration page.

Once you have determined the IP address that the SAMA5 has been leased (such as `192.168.1.100`), put the IP address into any web browser of a computer that is on the same local network as the SAMA5.

From there, you should see the SAMA5 login page, where you can enter the credentials:

- username: `sama5`
- password `sama5`

Once logged in, you will see 3 large buttons. Assuming you have not configured your [Iotery](https://iotery.io) account to work with the demo, press the _*Start*_ button. Follow the directions on the screen. This will walk you through creating the necessary types and SAMA5 device on your account.

Once you complete the demonstration, you should be able to turn the LED on and off commanding the docker firmware container running on the SAMA5 from your [Iotery Device Dashboard](https://iotery.io/devices)!

-->

## Connecting to the SAMA5 OS

There are two typical ways to connect with the operating system and work on the SAMA5 directly using the command line:

1. JLINK - Connecting the SAMA5 development board to a computer using the **JLINK** micro USB socket (located next to the Ethernet socket) provides a serial TTY port over USB. You can connect to this port using a telnet application such as [PuTTY](https://www.putty.org/) or [picocom](https://github.com/npat-efault/picocom). The advantage of using the JLINK connection is that system messages and `stdout` are printed to the terminal. This is very useful for debugging but can be problematic when trying to interact with an application.

2. SSH - Once you determine the IP address of the SAMA5, you can connect using SSH on port 22 (standard). This can be done from the command line (for example `ssh sama5@192.168.10.100`) or with an application such as [PuTTY](https://www.putty.org/). The advantages of using SSH are that it can be used remotely and system messages are not printed to the terminal. This make it easier to work with applications.

The SAMA5 images included in the repository come with two **user accounts** that can be used to connect:

1. root - the system account; password: *sama5*; directory: `/root`
2. sama5 - the default user account; password: *sama5*; directory: `/home/sama5`; requires using the `su` command for root privileges

## SAMA5 Firmware Structure

The Iotery SAMA5 has 3 primary layers:

1. Kernel & OS Layer - Linux
2. Runtime Layer - Iotery `boot.py` and `main.py` files
3. Application Layer - Your code (like Docker or a simple Python script)

### Kernel & OS Layer

The SAMA5 images included in the repository are built using [Busybox](https://busybox.net/about.html) and (currently) Linux kernel is 4.19. They include a basic functional set of standard utilities and [Python3](https://www.python.org/). Builds that include [Docker](https://www.docker.com) are more extensive. The images are fully installed (partitions, swap, users, etc.) so that they are flash-and-go ready.

<!--

The SAMA5 image included in this repository has a Debian based Linux (4.13 kernel) built with [busybox](https://busybox.net/about.html). Building your own..._*HELP*_

-->

### Runtime Layer

Iotery provides a flat runtime layer which is designed to be simple and transparent. It can launch your applications after boot without requiring modifications to the OS layer.

There are two key runtime files, `boot.py` and `main.py`, which are always in the `/home/sama5` user directory. These **Python3** files are run by by the OS after the boot sequence has completed.

1. `boot.py` - This Python3 script is run first using a *WAIT* call. This means that the OS calls the script and then waits (i.e. stops) until the script returns. This script **MUST** return or the runtime layer will freeze. Use this script to accomplish setup tasks like clearing the LEDs, setting up COM channels, or starting a detached process. 

2. `main.py` - This Python3 script is run after `boot.py` using a *ONCE* call. This means that the OS calls the script one time and moves on, not waiting for a return. Use this script to launch your main processes.  

The runtime layer is provided as a convenience to the end user. Feel free to edit `boot.py` and `main.py` as needed. However, we recommend for simplicity that the runtime layer scripts be used to launch applications rather than being used as the main application. The scripts are called from `/etc/inittab` which is part of the Busybox init system. If need be they can be bypassed by editing the `inittab` file.

In addition to `boot.py` and `main.py`, the runtime layer for the demo includes a small python-based WSGI webserver in `/home/sama5/server`, a shared Python config module in `/home/sama5/shared/config`, and a shared Python logging in `/home/sama5/shared/logs`. These are used by both the demo runtime layer and the demo application layer to store Iotery credentials and log activity. They may be useful for similar applications.

<!--

All runtime files are located in the `/home/sama5`


Iotery provides a runtime layer designed to be simple and flat that is responsible for running and launching your application files such as an application-specific python file that launches your docker daemon or a simply python script.

There are two primary files that are important located in the `/home/sama5/????` folder:

1. `boot.py` - Handles booting the necessary basic functions and is launched from `init.d`.
2. `main.py` - Handles either launching a python script, running specific code, or launching a Docker daemon.

Feel free to edit these. It is probably easiest to leave them, and modify the launch script that is launched from `main.py`.

There are also some auxiliary files that will make life easier, too:

1. Logging: `sama5.log` - where all output from the runtime layer is stored.
2. Iotery config: `iotery.json` - where the credentials for your SAMA5 device are for your Iotery connection.

-->

### Application Layer

The application layer is typically the layer where your application will be stored and run. This could be anything, such as a Python program, bash scripts, or C++ objects (to name a few). This may also be where you run a script that boots and manages a Docker instance as a firmware container. This is how the LED Demo functions.

For the LED Demo, the [`iotery_updater.py`](https://gitlab.com/iotery-sama5/sama5-led-demo/blob/master/image_source/home/sama5/iotery/iotery_updater_v1.py) script is our first-level application. It is launched by `main.py` (which then closes), and in turn it launches and manages a docker container hosting the second-level application (the firmware). The updater's job is simply to keep itself and the Docker container running (a watchdog) and to download image updates and restart the container as needed. The Docker container runs the [`iotery_demo1_robust.py`](https://gitlab.com/iotery-sama5/sama5-led-demo/blob/master/image_source/home/sama5/iotery/iotery_demo1_robust.py) firmware script which connects Iotery to the SAMA5 hardware (the LEDs).

<!--
application which interacts with Iotery.

we have an [`updater.py`](this-repos-updater-script-link) script that is launched from `main.py` that manages the docker firmware image that runs and reports data and process LED commands from Iotery, as well as acting as a watchdog and update listener.

-->

## Changing the Firmware

In this repository, you will find a simple [Dockerfile](https://gitlab.com/iotery-sama5/sama5-led-demo/blob/master/image_source/home/sama5/docker/Dockerfile) that contains the information needed to duplicate the Docker image we are using for the demo. You can expand on this basic model to create a new image that is specific to your situational needs. This can be done by using Git to fork this repository, making the necessary modifications, and then pushing the new image to your own container registry. Read more about Gitlab's container registry [here](https://docs.gitlab.com/ee/user/packages/container_registry/) - it's free!

Once you have a new Docker image, you can launch/run/maintain it using the [`iotery_updater.py`](https://gitlab.com/iotery-sama5/sama5-led-demo/blob/master/image_source/home/sama5/iotery/iotery_updater_v1.py) script with only slight modifications. You will need to change the `UPDATER.docker_image` variable to the address of your container registry, and change the `UPDATER.docker_version` variable to match the tag or your initial image. You also need to verify that the start variables in `UPDATER.start_one_image` match your setup.

<!--

shows all the code on how the SAMA5 LED demo firmware connects to Iotery, sends data, and executes commands. For more information on Iotery, [check out the docs](https://iotery.io/docs). If you want to modify the firmware, fork the repository and modify the Dockerfile and the files it uses, push to your own container registry (like the container registry in which the current [firmware image is](https://gitlab.com/registry-location-of-led-demo) ). Read more about Gitlab's container registry [here](https://docs.gitlab.com/ee/user/packages/container_registry/) - it's free!

-->

## Building the Linux Image

The Linux images in this repository were built using [Buildroot](https://buildroot.org/). 

### Basic Build

For a basic Linux build, follow the MicroChip instructions located [here](https://github.com/linux4sam/buildroot-external-microchip). Following these instructions exactly will provide a basic image suitable for running many applications. However, you may want to run `make menuconfig` prior to running `make` and add some additional packages. Be sure to read the top-level `<Help>` tab inside `make menuconfig` to learn how to use all the search, shortcut, and dependencies features. Here are some packages to consider:

1. python3 - you may be required to disable python (python2) in order to add python3
2. python3 internal libraries - sqlite and maybe other (all) 
2. python3 external libraries - pip and others
3. util-linux - basic set and any partition tools needed
4. e2fsprogs - for expanding partitions
5. rsync - for transferring files to the device
6. ntp - keeps device time up-to-date


### Building for Docker

Building an image that will run Docker is more complex as it requires many more kernel modules. Following the above build, the simplest and most up-to-date way to determine what modules your kernel build needs is to use Docker's [check-config.sh](https://github.com/moby/moby/raw/master/contrib/check-config.sh) script. Run this script on the kernel `.config` file in a manner similar to this:

`./check-config.sh buildroot-at91/output/build/linux-linux4sam_6.2-rc3/.config`

This will provide a list of necessary kernel modules. You can then use the search function inside `make linux-menuconfig` to locate the modules and their dependencies. Note that currently Docker will only use some networking modules if they are not built-ins.


### Post Build

After building there are several steps needed to create a stable image, particularly if using Docker:

1. expand the image partition to 3GB+ 
2. create and install a swap file or swap partition 2x system memory
3. create `/etc/sysctl.conf` - see busybox sysctl
3. set vm.min_free_kbytes = 4096 - see busybox sysctl
4. set vm.swappiness = 25 - see busybox sysctl
5. add `::sysinit:/sbin/sysctl -p` to `/etc/inittab` - loads `/etc/sysctl.conf`



































