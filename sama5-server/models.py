from pydantic import BaseModel
from typing import List


class User(BaseModel):
    username: str = None
    password: str = None


class Network(BaseModel):
    current_local_ip: str = None
    configuration: str = "DHCP"
    network_interface: str = 'eth0'


class Device(BaseModel):
    serial: str = ""
    key: str = ""
    secret: str = ""
    name: str = ""
    network: Network = Network()
