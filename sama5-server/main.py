from starlette.middleware.cors import CORSMiddleware
from fastapi import Depends, FastAPI, APIRouter
from starlette.responses import RedirectResponse, HTMLResponse, FileResponse
from starlette.staticfiles import StaticFiles
from starlette.requests import Request
from starlette.responses import Response
from models import User, Device
from utils import auth
import pkg_resources
from os import path
from routes.device_routes import handle_device_update, handle_device_get, handle_device_logs_get
from typing import List

app = FastAPI(title="Iotery SAMA5")


# CORS ONLY NEEDED FOR LOCAL DEV
origins = [
    "http://localhost",
    "http://localhost:3000",
    "http://localhost:8000"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/api/user", tags=["User"])
async def entry(user: User = Depends(auth)):
    # Responsible for getting the user information from the Basic Auth creds
    print(user)
    return {"hello": user.username}


@app.patch("/api/user", tags=["User"])
async def update_user(new_user: User = None, user: User = Depends(auth)):
    # Responsible for updating the user information from the Basic Auth creds
    return {"not": "implemented"}


@app.get("/api/status", tags=["Device"])
async def get_status(user: User = Depends(auth)):
    # Responsible for getting the device status
    return {"status": "ok"}


@app.get("/api/device", tags=["Device"])
async def get_device(user: User = Depends(auth)):
    # Responsible for getting the iotery device credentials
    device = handle_device_get()
    return device


@app.patch("/api/device", tags=["Device"])
async def update_device(device: Device, user: User = Depends(auth)):
    # Responsible for updating the iotery device credentials
    device = Device()

    handle_device_update(device)

    return device


@app.get("/api/logs", tags=["Device"])
async def get_logs(user: User = Depends(auth)):
    # Responsible for getting the logs

    logs: List[str] = handle_device_logs_get()

    return logs


# Serves HTML react app
# Lots of crappy handling because of SPA (and react-router)
@app.get("/.*", include_in_schema=False)
def root(request: Request, user: User = Depends(auth)):
    if request.url.path == "/":
        return FileResponse(f'frontend/build/index.html')
    else:
        if path.exists(f'frontend/build{request.url.path}'):
            return FileResponse(f'frontend/build{request.url.path}')
        else:
            return FileResponse(f'frontend/build/index.html')
