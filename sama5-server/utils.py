
from fastapi import Depends, HTTPException
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from starlette.status import HTTP_401_UNAUTHORIZED

from models import User

security = HTTPBasic()


async def auth(creds: HTTPBasicCredentials = Depends(security)):
    u = User(username=creds.username, password=creds.password)

    # Do your authentication check here:
    if creds.username != "sama5" or creds.password != "sama51":
        raise HTTPException(
            status_code=HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Basic"},
        )
    else:
        return u
