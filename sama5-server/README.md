# SAMA5 Modern Server Example

The following is an example of modern server and frontend that can be used with the SAMA5 management configuration if a more flexible, modern local configuration is needed on your device.

The server is powered by [FastAPI](https://github.com/tiangolo/fastapi), which is built upon [Starlette](https://starlette.io).

The frontend is built using [React](https://reactjs.org).

## Where it can be used

This folder takes the place of the `sama5/server` folder located in `/image_source`.

## Running

To run the server, install the dependencies:

```
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

This will install Uvicorn, which runs the server: `main.py`. To run:

```
uvicorn main:app --reload --port 80
```

This will start the server with hot reload on port 80, as well as serve up the built frontend located in the `frontend/build` folder.

## Developing the frontend

To develop on the frontend, you will need `nodejs` installed with `yarn`. `cd` into the frontend directory and:

```
yarn install
```

and then

```
yarn start
```

to start the frontend. The frontend will start on port 3000 by default. To connect to the backend server runnin on port 8000, you will need to set env vars:

```
export REACT_APP_BASE_URL=http://localhost:8000;
export REACT_APP_TEST_TOKEN=your basic auth header value;
```

#### Including in your server build

You will need to `build` the frontend in order for it to be included in the server. To build:

```
yarn run build
```

This will create an optimized `build` folder with all necessary dependencies to be served by the FastAPI server.
