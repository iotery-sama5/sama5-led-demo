let baseUrl = process.env.REACT_APP_BASE_URL || "";
let testToken = process.env.REACT_APP_TEST_TOKEN || null;

export const get = async ({ route }) => {
  let res = await fetch(`${baseUrl}${route}`, {
    headers: {
      //   Authorization: testToken ? `Basic ${testToken}` : undefined,
      "Content-Type": "application/json"
    },
    credentials: "include"
  });

  if (res.status >= 400) {
    alert(JSON.stringify(res));
    return;
  }

  return await res.json();
};

export const patch = async ({ route, data }) => {
  let res = await fetch(`${baseUrl}${route}`, {
    method: "PATCH",
    headers: {
      //   Authorization: testToken ? `Basic ${testToken}` : undefined,
      "Content-Type": "application/json"
    },
    credentials: "include",
    body: JSON.stringify(data)
  });

  if (res.status >= 400) {
    alert(JSON.stringify(res));
    return;
  }

  return await res.json();
};
