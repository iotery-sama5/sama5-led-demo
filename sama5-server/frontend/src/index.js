import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./screens/App";
import Status from "./screens/Status";
import Device from "./screens/Device";
import Logs from "./screens/Logs";
import NotFound from "./404";
import * as serviceWorker from "./serviceWorker";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Wrapper from "./shared/Wrapper";

ReactDOM.render(
  <Router>
    <Switch>
      <Route path="/" exact component={App} />
      <Wrapper>
        <Route path="/status" component={Status} />
        <Route path="/logs" component={Logs} />
        <Route path="/device" component={Device} />
      </Wrapper>
      <Route component={NotFound} />
    </Switch>
  </Router>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
