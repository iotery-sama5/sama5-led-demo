import React, { useState, useEffect } from "react";
import styles from "./logs.module.css";
import { get } from "../../utils/api";
import loader from "../../assets/loader.gif";

function Logs() {
  const [logs, setLogs] = useState(null);

  useEffect(() => {
    get({ route: "/api/logs" }).then(res => {
      setLogs(res);
    });
  }, []);

  if (!logs) {
    return (
      <div className="loader">
        <img src={loader}></img>
      </div>
    );
  }

  return (
    <>
      <h1>SAMA5 Logs</h1>
      <div className={styles.code}>
        {logs.map(l => (
          <div class={styles.code_line}>{l}</div>
        ))}
        <div className={styles.dots}></div>
      </div>
    </>
  );
}

export default Logs;
