import React, { useState, useEffect } from "react";
import s from "./status.module.css";
import { get } from "../../utils/api";
import loader from "../../assets/loader.gif";

function Status() {
  const [user, setUser] = useState(null);
  const [device, setDevice] = useState(null);
  useEffect(() => {
    get({ route: "/api/user" }).then(res => {
      setUser(res);
    });

    get({ route: "/api/device" }).then(res => {
      setDevice(res);
    });
  }, []);

  if (!device || !user) {
    return (
      <div className="loader">
        <img src={loader}></img>
      </div>
    );
  }

  return (
    <div className={s.App}>
      This is status, user is {user ? user.hello : null}
    </div>
  );
}

export default Status;
