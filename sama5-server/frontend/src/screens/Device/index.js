import React, { useState, useEffect } from "react";
import s from "./device.module.css";
import { get, patch } from "../../utils/api";
import loader from "../../assets/loader.gif";

function Device() {
  const [user, setUser] = useState(null);
  const [device, setDevice] = useState(null);
  const [valueIsUpdating, setUpdatingValue] = useState(null);
  const [needsToUpdate, setNeedsToUpdate] = useState(false);

  function updateDeviceConfigs() {
    patch({
      route: "/api/device",
      data: {
        name: device.name,
        serial: device.serial,
        key: device.key,
        secret: device.secret
      }
    }).then(res => {
      setNeedsToUpdate(false);
    });
  }

  function cancelUpdate() {
    get({
      route: "/api/device"
    }).then(res => {
      setDevice(res);
      setNeedsToUpdate(false);
    });
  }

  const handleChange = name => ev => {
    let d = device;
    device[name] = ev.target.value;
    setDevice(device);
    setNeedsToUpdate(true);
  };

  const onBlur = name => ev => {
    console.log(device);
    setUpdatingValue(null);
  };

  const handleClick = name => ev => {
    setUpdatingValue(name);
    console.log(document.getElementById(name).childNodes[0]);
    document.getElementById(name).childNodes[0].focus();
  };

  useEffect(() => {
    get({ route: "/api/device" }).then(res => {
      setDevice(res);
    });
  }, []);

  if (!device) {
    return (
      <div className="loader">
        <img src={loader}></img>
      </div>
    );
  }

  return (
    <>
      <h1>Device Identity</h1>
      <p>
        The identity of the device are the same credentials that should be set
        in{" "}
        <a href="https://iotery.io/devices" target="_blank">
          Iotery
        </a>
        . These credentials are saved internally to and identity file that can
        be read by your applications to authenticate with Iotery.
      </p>
      <KeyValueContainer>
        <Row>
          <ItemKey>Name</ItemKey>
          <ItemValue
            id="name"
            isEditing={valueIsUpdating === "name"}
            onClick={handleClick("name")}
            onChange={handleChange("name")}
            onBlur={onBlur("name")}
          >
            {device.name}
          </ItemValue>
        </Row>
        <Row>
          <ItemKey>Serial</ItemKey>
          <ItemValue
            id="serial"
            isEditing={valueIsUpdating === "serial"}
            onClick={handleClick("serial")}
            onChange={handleChange("serial")}
            onBlur={onBlur("serial")}
          >
            {device.serial}
          </ItemValue>
        </Row>
        <Row>
          <ItemKey>Key</ItemKey>
          <ItemValue
            id="key"
            isEditing={valueIsUpdating === "key"}
            onClick={handleClick("key")}
            onChange={handleChange("key")}
            onBlur={onBlur("key")}
          >
            {device.key}
          </ItemValue>
        </Row>
        <Row>
          <ItemKey>Secret</ItemKey>
          <ItemValue
            id="secret"
            isEditing={valueIsUpdating === "secret"}
            onClick={handleClick("secret")}
            onChange={handleChange("secret")}
            onBlur={onBlur("secret")}
          >
            ••••••••••••••••
          </ItemValue>
        </Row>
      </KeyValueContainer>
      {needsToUpdate ? (
        <div className={s.action_container}>
          <a href="#" onClick={cancelUpdate}>
            cancel
          </a>
          <button onClick={updateDeviceConfigs}>Save</button>
        </div>
      ) : null}
    </>
  );
}

const KeyValueContainer = ({ children }) => {
  return <div className={s.keyValueContainer}>{children}</div>;
};

const Row = ({ children }) => {
  return <div className={s.row}>{children}</div>;
};

const ItemKey = ({ children }) => {
  return <div className={s.rowItemKey}>{children}</div>;
};

const ItemValue = ({ children, isEditing, ...props }) => {
  //   if (isEditing) {
  //     return (
  //       <div className={s.rowItemValue}>
  //         <input placeholder={children} {...props}></input>
  //       </div>
  //     );
  //   }

  return (
    <div className={s.rowItemValue} {...props}>
      <input
        style={{ display: isEditing ? undefined : "none" }}
        placeholder={children}
        {...props}
      ></input>
      <span style={{ display: isEditing ? "none" : undefined }}>
        {children}
      </span>
    </div>
  );
};

export default Device;
