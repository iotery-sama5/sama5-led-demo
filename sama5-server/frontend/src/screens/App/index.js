import React from "react";
import logo from "../../assets/logo.png";
import "./App.css";

function App(props) {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <div className="subheader">Iotery SAMA5 Development Board</div>
        <div className="link-container">
          <a
            className="App-link"
            href="https://iotery.io"
            target="_blank"
            rel="noopener noreferrer"
          >
            Iotery
          </a>
          <a
            className="App-link"
            // onClick={() => {
            //   props.history.push("/status");
            // }}
            rel="noopener noreferrer"
          >
            Get Started
          </a>
          <a
            className="App-link"
            href="#"
            onClick={e => {
              e.preventDefault();
              props.history.push("/status");
            }}
          >
            System Status
          </a>
        </div>
      </header>
    </div>
  );
}

export default App;
