import React, { useState } from "react";
import styles from "./wrapper.module.css";
// import classnames from "classnames";
import logo from "../../assets/icons/iotery.png";
import useReactRouter from "use-react-router";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faNetworkWired,
  faSdCard,
  faDatabase,
  faCogs,
  faMicrochip,
  faScroll
} from "@fortawesome/free-solid-svg-icons";

const Menu = props => {
  const { isOpen, toggleMenuState } = props;
  const { history } = useReactRouter();

  return (
    <menu className={isOpen ? styles.open : styles.closed}>
      <ul>
        <li className={styles.title}>
          <div
            className={styles.title_left}
            onClick={() => {
              history.push("/");
            }}
          >
            <img src={logo} /> Iotery SAMA5
          </div>
          <div className={styles.title_right} onClick={() => toggleMenuState()}>
            ☰
          </div>
        </li>
        <li
          onClick={() => {
            history.push("/device");
            toggleMenuState();
          }}
        >
          <FontAwesomeIcon icon={faMicrochip}></FontAwesomeIcon> Device Identity
        </li>
        <li
          onClick={() => {
            history.push("/status");
            toggleMenuState();
          }}
        >
          <FontAwesomeIcon icon={faSdCard}></FontAwesomeIcon> Status
        </li>
        <li>
          <FontAwesomeIcon icon={faNetworkWired}></FontAwesomeIcon> Network
        </li>
        <li>
          <FontAwesomeIcon icon={faDatabase}></FontAwesomeIcon> Data
        </li>
        <li
          onClick={() => {
            history.push("/logs");
            toggleMenuState();
          }}
        >
          <FontAwesomeIcon icon={faScroll}></FontAwesomeIcon> Logs
        </li>
        <li>
          <FontAwesomeIcon icon={faCogs}></FontAwesomeIcon> Iotery Setup
        </li>
      </ul>
    </menu>
  );
};

export default ({ props, children }) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggleMenuState = () => {
    setIsOpen(!isOpen);
  };
  return (
    <>
      <Menu isOpen={isOpen} toggleMenuState={toggleMenuState} {...props}></Menu>
      <div className={styles.container}>
        <header>
          <span
            className={styles.menu_button}
            onClick={() => toggleMenuState()}
          >
            ☰
          </span>
          <span className={styles.menu_title}>Iotery SAMA5</span>
        </header>
        <div className={styles.content}>{children}</div>
      </div>
    </>
  );
};
