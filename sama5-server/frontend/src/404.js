import React from "react";
import logo from "./assets/logo.png";
import "./screens/App/App.css";

function NotFound(props) {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <div className="subheader">Not Found. Sorry.</div>
        <div className="link-container">
          <a
            className="App-link"
            href="https://iotery.io"
            target="_blank"
            rel="noopener noreferrer"
          >
            Iotery
          </a>
        </div>
      </header>
    </div>
  );
}

export default NotFound;
