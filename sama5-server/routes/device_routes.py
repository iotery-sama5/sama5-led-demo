

from models import Device


def handle_device_update(device):

    print("Do your update here!")
    # Do your update here!

    return device


def handle_device_get():
    return Device(name="Device Name", serial="serial", key="key")


def handle_device_logs_get():

    # fake logs - edit them here
    logs = ["2019-11-02 21:18:21 EDT BOOT INFO 273 Iotery config server started. Pid: 287",
            "2019-11-02 21:18:25 EDT MAIN INFO 291 Iotery updater script started. Pid: 309",
            "2019-11-02 21:18:21 EDT BOOT INFO 273 Iotery config server started. Pid: 287",
            "2019-11-02 21:18:25 EDT MAIN INFO 291 Iotery updater script started. Pid: 309",
            "2019-11-02 21:18:21 EDT BOOT INFO 273 Iotery config server started. Pid: 287",
            "2019-11-02 21:18:25 EDT MAIN INFO 291 Iotery updater script started. Pid: 309",
            "2019-11-02 21:18:21 EDT BOOT INFO 273 Iotery config server started. Pid: 287",
            "2019-11-02 21:18:25 EDT MAIN INFO 291 Iotery updater script started. Pid: 309",
            "2019-11-02 21:18:21 EDT BOOT INFO 273 Iotery config server started. Pid: 287",
            "2019-11-02 21:18:25 EDT MAIN INFO 291 Iotery updater script started. Pid: 309"]

    return logs
