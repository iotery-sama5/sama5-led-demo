# /home/sama5/main.py
# This script is run by the init system after running /home/sama5/boot.py.
# This script WILL be run with ROOT privileges.
# This script does not need to return, and it will not respawn.
# See: /etc/inittab @ ::once:/usr/bin/python3 /home/sama5/main.py

# notify start
print('Run main.py')

# build logger
import sys
sys.path.append('/home/sama5/shared/logs')
import shared_logger
logger = shared_logger.SharedLogger()
logger.pname = 'MAIN'
logger.show_logs = True

# start iotery demo (docker option) i.e. start docker via the update script
from subprocess import Popen,DEVNULL
updater_pid = Popen(['/usr/bin/python3','/home/sama5/iotery/iotery_updater_v1.py'],
                     shell=False,close_fds=True,
                     #stdin=DEVNULL,stdout=DEVNULL,stderr=DEVNULL).pid
                     stdin=None,stdout=None,stderr=None).pid
logger.log('Iotery updater script started. Pid: {}'.format(updater_pid),'info')

# notify end
print('End main.py')





