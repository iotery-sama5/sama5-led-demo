# /home/sama5/boot.py
# This script is run by the init system prior to running /home/sama5/main.py.
# This script WILL be run with ROOT privileges.
# This script MUST return or the boot sequence will hang.
# See: /etc/inittab @ ::wait:/usr/bin/python3 /home/sama5/boot.py

# notify start
print('Run boot.py')

# turn off LEDs then blink R,G,B
try:
    import time
    from mpio import LED
    for color in ('red','green','blue'):
        LED(color).brightness = 0
        print('LED {} OFF'.format(color))
except:
    pass

# build logger
import sys
sys.path.append('/home/sama5/shared/logs')
import shared_logger
logger = shared_logger.SharedLogger()
logger.pname = 'BOOT'
logger.show_logs = True

# start server
from subprocess import Popen,DEVNULL
server_pid = Popen(['/usr/bin/python3','/home/sama5/server/simple_server.py'],
                   shell=False,close_fds=True,
                   #stdin=DEVNULL,stdout=DEVNULL,stderr=DEVNULL).pid
                   stdin=None,stdout=None,stderr=None).pid
logger.log('Iotery config server started. Pid: {}'.format(server_pid),'info')

# notify end
print('End boot.py')

