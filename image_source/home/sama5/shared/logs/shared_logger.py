#!/usr/bin/python3

#-----------------------------------------------------------------------
# notes
#-----------------------------------------------------------------------

# Simple shared logger script 'shared_logger.py'.

# This logs to ./shared_logger.log.
# This uses the Python3 logger module as logger.getLogger('shared_logger').
# This rotates logs once they reach aproximately 1MB.
# This keeps 2 previous log file.

import os,time,logging
from logging.handlers import RotatingFileHandler

class SharedLogger:

    def __init__(self):
        '''Init function. No input.
        Set variables directly if needed.
        '''
        
        # paths (make sure they are absolute)
        self.shared_log_dir = os.path.dirname(os.path.abspath(__file__))
        self.shared_log_file = os.path.join(self.shared_log_dir,'shared_logger.log')

        # create logger
        self.logger = logging.getLogger('shared_logger')
        self.logger.setLevel(logging.DEBUG)
        self.logger.addHandler(RotatingFileHandler(self.shared_log_file,maxBytes=1024000,backupCount=2))

        # variables
        self.show_logs = True
        self.pid = os.getpid()
        self.pname = 'PID{}'.format(self.pid)

    def log(self,msg,msg_type='info'):
        '''Write msg to shared log.
        msg_type=notset|debug|info|warning|error|critical
        '''

        # fix message
        msg = str(msg).strip()

        # fix message type --> log level
        msg_type = str(msg_type).upper()
        levels = ('NOTSET','DEBUG','INFO','WARNING','ERROR','CRITICAL')
        if msg_type not in levels:
            msg_type = 'NOTSET'
        level = levels.index(msg_type)*10

        # don't log notset messages
        if msg_type == 'NOTSET':
            if self.show_logs:
                print('{} {} {}'.format(self.pname,self.pid,msg))

        # add more print specificity here

        # do log (not using logging format)
        else:
            if self.show_logs:
                print('{} {} {}'.format(self.pname,self.pid,msg))
            self.logger.log(level,'{} {} {} {} {}'.format(time.strftime('%Y-%m-%d %H:%M:%S %Z'),self.pname,msg_type,self.pid,msg))


    def tail(self,linecount=32,reverse=False):
        '''Return a string of the last "linecount" lines of log.'''

        lines = []

        try:
            lc = 0
            with open(self.shared_log_file) as f:
                for line in f:
                    line = line.strip()
                    if line:
                        lines.append(line)
                        lc += 1
                        if lc > linecount:
                            lines.pop(0)
            if lc == 0:
                lines.append('Log file has no data.')

        except:
            import traceback
            lines.append(traceback.format_exc())
            lines.append('Error reading log file.')

        if reverse:
            lines.reverse()

        return '\n'.join(lines)

