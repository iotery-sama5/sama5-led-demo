#!/usr/bin/python3

#-----------------------------------------------------------------------
# notes
#-----------------------------------------------------------------------

# Simple shared config script 'shared_config.py'.

# This uses the JSON file ./shared_config.json.

import os,time,json,traceback

class SharedConfig:

    def __init__(self):
        '''Init function. No input.
        Set variables directly if needed.
        '''
        
        # paths (make sure they are absolute)
        self.shared_config_dir = os.path.dirname(os.path.abspath(__file__))
        self.shared_config_file = os.path.join(self.shared_config_dir,'shared_config.json')

        # create file
        if not os.path.isfile(self.shared_config_file):
            self.write({'status':'NEW {}'.format(time.strftime("%a %d %b %Y %H:%M:%S %Z",time.localtime()))})

    @property
    def mtime(self):
        '''Return mod time of config file.'''
        
        return os.path.getmtime(self.shared_config_file)

    def read(self):
        '''Return a dict of config data.'''

        config_data = {}

        try:
            jdata = ''
            with open(self.shared_config_file) as f:
                jdata = f.read().strip()
                f.close()
            if jdata:
               config_data = json.loads(jdata)
            config_data.pop('error',None)

        except:
            message = ''
            message += traceback.format_exc()
            message += 'CONFIG ERROR: Could not read config data.'
            print(message)
            config_data = {'error':message}

        return config_data

    def update(self,data):
        '''Update config file with values from dict 'data'.
        Return status True or False.'''

        olddata = self.read()

        if 'error' in data:
            return False

        olddata['status'] = 'UPDATE {}'.format(time.strftime("%a %d %b %Y %H:%M:%S %Z",time.localtime()))

        for item,value in data.items():
            olddata[item] = value

        return self.write(olddata)

    def write(self,data):
        '''Overwrite data (a dict) to config file.
        Return status True or False.'''

        try:

            # do this before opening file in case of failure
            # otherwise failure will leave a blank file
            data = json.dumps(data,indent=4)
            
            with open(self.shared_config_file,'w') as f:
                f.write(data)
                f.close()
                
            return True

        except:
            import traceback
            print(traceback.format_exc())
            print('Config WRITE error.')
            return False























