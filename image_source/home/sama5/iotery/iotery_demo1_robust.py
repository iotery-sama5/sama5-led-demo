#!/usr/bin/python3

#-----------------------------------------------------------------------
# notes
#-----------------------------------------------------------------------

# Iotery example file 'iotery_demo1_robust.py'.

# This a more robust example of using Python3 + Iotery Embedded SDK to connect to the iotery.com API.
# This will attempt to re-establish a connection with Iotery as long as only IoteryException is raised.
# This will also monitor the shared data file "shared/iotery_setup.json" for credential changes.

# This is a more complex example with some error handling.
# It is intended for illustration (lots of notes) and debugging.
# For a more simple and straightforward example see 'iotery_demo1_simple.py'.

# If the mpio module is available, the green LED indicates Iotery connection,
# and the blue LED indicates command execution. The red LED indicates error. 

# short delay before doing anything
# allows device to become stable

import time
print('Demo start delay: 10 seconds.')
time.sleep(10)

#-----------------------------------------------------------------------
# imports
#-----------------------------------------------------------------------

# internal imports
import os,sys,time,traceback

# external packages (available from PyPi using pip)
# REQUIRED: iotery-embedded-python-sdk     ~ python3 -m pip install iotery-embedded-python-sdk
# OPTIONAL: Microchip mpio for LED control ~ python3 -m pip install mpio

# Iotery SDK
from iotery_embedded_python_sdk import Iotery,IoteryException

# Microchip MPIO
try:
    from mpio import LED
    mpio_ok = True
except:
    mpio_ok = False

# shared paths
thisdir   = os.path.dirname(os.path.abspath(__file__))
parentdir = os.path.dirname(thisdir)
sharedir  = os.path.join(parentdir,'shared')
logdir    = os.path.join(sharedir,'logs')
configdir = os.path.join(sharedir,'config')

# logger and config
sys.path.append(logdir)
sys.path.append(configdir)
import shared_config,shared_logger
logger = shared_logger.SharedLogger()
config = shared_config.SharedConfig()
logger.pname = 'DEMO'
logger.show_logs = True

#-----------------------------------------------------------------------
# example run function
#-----------------------------------------------------------------------

def run():
   
    # instantiate
    demo = DEMO1R()

    # set variables if needed
    demo.loop_every = 2 # seconds
    demo.show_response = True

    # start loop
    demo.run_forever()       

#-----------------------------------------------------------------------
# main class
#-----------------------------------------------------------------------

class DEMO1R:
    '''Iotery Embedded Python SDK Demo 1 Robust
    '''

    # static user variables
    loop_every = 10 # seconds
    on_error_restart = 10 # seconds
    show_response = True
    # show_logs = True # see logger setup

    # function variables
    shared_data_file_mtime = 0    

    def __init__(self):
        '''Init function. No input.
        Set variables directly if needed.
        '''

        # setup shared logger
        # logger.pname = 'DEMO' # see logger setup
        # logger.show_logs = self.show_logs # see logger setup
        self.log = logger.log
        self.log('INIT','info')

        # Iotery SDK
        self.iotery = Iotery()

        # MPIO
        if not mpio_ok:
            self.log('MPIO not available.','info')

        # clear (turn off) LEDs
        self.led('all',0)

    def run_forever(self):
        '''Main loop. Robust. Attempts re-connect on iotery error.'''

        # catch script errors
        # log error then re-raise same error
        try:

            # re-clear (turn off) LEDs
            self.led('all',0)

            # main loop
            while 1:

                # init status
                config.update({'status_demo':'not connected'})

                # read config/shared data
                shared_data = config.read()
                self.shared_data_file_mtime = config.mtime

                # no data
                if not shared_data:
                    config.update({'status_demo':'config data missing'})
                    self.log('No shared data. Restart in {} seconds.'.format(self.on_error_restart),'debug')
                    time.sleep(self.on_error_restart)
                    continue

                # parse data
                teamid       = shared_data.get('teamUuid'    ,None)
                devicename   = shared_data.get('deviceName'  ,None)
                deviceserial = shared_data.get('deviceSerial',None)
                devicekey    = shared_data.get('deviceKey'   ,None)
                devicesecret = shared_data.get('deviceSecret',None)

                # missing data
                missing = [x for x in (teamid,deviceserial,devicekey,devicesecret) if not x]
                if missing:
                    config.update({'status_demo':'config data missing'})
                    self.log('Missing config items. Restart in {} seconds.'.format(self.on_error_restart),'debug')
                    time.sleep(self.on_error_restart)
                    continue

                # get auth token from Iotery
                try:

                    # required upload data
                    upload_data = {"key":devicekey,
                                   "serial":deviceserial,
                                   "secret":devicesecret,
                                   "teamUuid":teamid}

                    # make request (will raise error if auth fails)
                    self.led('green',1)
                    device_data = self.iotery.getDeviceTokenBasic(data=upload_data)
                    self.led('green',0)

                    # returned token data
                    token = device_data['token']
                    token_expiration = device_data['expiration']

                    # set auth token in SDK (used on every call to Iotery)
                    self.led('green',1)
                    self.iotery.set_token(token)
                    self.led('green',0)

                    # get Iotery data about device
                    self.led('green',1)
                    my_data = self.iotery.getMe()
                    self.led('green',0)
                    device_name = my_data['name']
                    device_uuid = my_data['uuid']
                    device_type_uuid = my_data['deviceType']['uuid']

                    # drop unused data
                    del device_data,my_data

                    # update shared data status
                    config.update({'status_demo':'connected'})
                    self.log('Connected to Iotery.','info')

                # Iotery error, continue will cause a re-connect
                except IoteryException:
                    self.led('all',0)
                    self.led('red',1)
                    config.update({'status_demo':'auth response error caused break'})
                    message  = 'Iotery auth response error.\n'
                    message += traceback.format_exc().strip()
                    message += '\nRestart in {} seconds.'.format(self.on_error_restart)
                    self.log(message,'error')
                    time.sleep(self.on_error_restart)
                    continue

                # non-Iotery errors will cause crash          

                # display collected data
                if self.show_response:
                    print('DEMO1 IOTERY TOKEN:',token)
                    print('DEMO1 TOKEN EXPIRATION:',token_expiration,time.strftime("%a %d %b %Y %H:%M:%S %Z",time.localtime(token_expiration)))
                    print('DEMO1 DEVICE NAME:',device_name)
                    print('DEMO1 DEVICE UUID:',device_uuid)
                    print('DEMO1 DEVICE TYPE UUID:',device_type_uuid)

                # auth success, go into secondary loop

                # secondary loop
                nextloop = 0
                loop2count = 0
                loop2_success = False 
                while 1:
                    loop2count += 1

                    # wait period
                    while time.time() < nextloop:
                        time.sleep(self.loop_every/100)
                    nextloop = time.time() + self.loop_every
                
                    # clear red LED (may have an error on)
                    self.led('red',0)

                    # check config
                    if self.shared_data_file_mtime != config.mtime:
                        shared_data = config.read()
                        self.shared_data_file_mtime = config.mtime
                        # need to re-connect to iotery
                        if (shared_data.get('teamUuid',None) and shared_data.get('teamUuid',None) != teamid) or\
                           (shared_data.get('deviceSerial',None) and shared_data.get('deviceSerial',None) != deviceserial) or\
                           (shared_data.get('deviceKey',None) and shared_data.get('deviceKey',None) != devicekey) or\
                           (shared_data.get('deviceSecret',None) and shared_data.get('deviceSecret',None) != devicesecret):
                            self.log('Config data has changed.','info')
                            break

                    # check token timeout
                    if time.time() > token_expiration - 600:
                        self.log('Iotery token expired.','info')
                        break

                    # make upload data
                    upload_data = self.upload_data_builder()
                    packet = {'timestamp':int(time.time()),
                              'deviceUuid':device_uuid,
                              'deviceTypeUuid':device_type_uuid,
                              'data':upload_data}

                    # catch errors
                    try:

                        # upload data
                        self.led('green',1)
                        response = self.iotery.postData(deviceUuid=device_uuid,data={"packets":[packet]})
                        self.led('green',0)

                        # show response
                        if self.show_response:
                            print('DEMO1 RESPONSE:',loop2count,response)

                        # handle commands
                        for command in response['unexecutedCommands']['device']:
                            self.command_handler(command)

                        # loop2 success
                        loop2_success = True

                        # reset config status
                        if shared_data.get('status_demo',None) != 'connected':
                            config.update({'status_demo':'connected'})
                            self.log('Connection to Iotery okay.','info')

                    # iotery error, break will cause a re-connect
                    except IoteryException:
                        self.led('all',0)
                        self.led('red',1)
                        config.update({'status_demo':'auth response error caused break'})
                        message  = 'Iotery auth response error.\n'
                        message += traceback.format_exc().strip()
                        message += '\nRestart in {} seconds.'.format(self.on_error_restart)
                        self.log(message,'error')
                        time.sleep(self.on_error_restart)
                        break

                    # other exceptions
                    except Exception as e:
                        self.led('all',0)
                        self.led('red',1)
                        message  = 'Unspecified class error.\n'
                        message += traceback.format_exc().strip()

                        # never had success, so allow failure/crash
                        if not loop2_success:
                            config.update({'status_demo':'unspecified class error (no restart)'})
                            message += '\nNo restart.'
                            self.log(message,'error')
                            raise e

                        # we've had success, so try again
                        else:
                            config.update({'status_demo':'unspecified class error'})
                            message += '\nRetry in {} seconds.'.format(self.on_error_restart)
                            self.log(message,'error')
                            time.sleep(self.on_error_restart)
                            continue

                    # success at this point == remain in secondary loop
                    # otherwise, break to main loop will cause re-connect

        # catch non-iotery-auth errors
        # log error then re-raise same error
        except Exception as e:
            self.led('all',0)
            self.led('red',1)
            config.update({'status_demo':'unspecified class error (no restart)'})
            message  = 'Unspecified class error.\n'
            message += traceback.format_exc().strip()
            message += '\nNo restart.'
            self.log(message,'error')
            raise e

    def upload_data_builder(self):
        '''Build data dict for upload to Iotery.
        This function should be replaced by a user function.
        '''

        # example data

        if mpio_ok and LED('blue').brightness:
            state = 1
        else:
            state = 0
        
        data = {'timestamp':int(time.time()),
                'script':'iotery_demo1_robust.py',
                'cpu_temp':0, # add CPU temp function here
                'local_time':time.strftime("%a %d %b %Y %H:%M:%S %Z",time.localtime()),
                'led_state':state,
                }

        return data

    def command_handler(self,command):
        '''Handle unexecuted command instances from Iotery.
        This function should be replaced by a user function.
        '''

        # necessary variables in command instance
        command_uuid  = command['uuid']
        ctype_uuid    = command['commandTypeUuid']
        ctype_enum    = command['commandTypeEnum']

        # option to control clearing command after execution
        command_processed = False
        command_clear = False

        # each command has a unique 'commandTypeUuid' that identifies it
        # get the 'commandTypeUuid' from Iotery when you set up the command
        # then you can use it to perform a very specific function
        if ctype_uuid == 'xxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx':
            command_processed = True
            command_clear = True

        # each command also has a 'commandTypeEnum' that identifies it
        # get the 'commandTypeEnum' from Iotery when you set up the command
        # then you can use it to perform a more general function

        elif ctype_enum == 'BLINK_SAMA5_LED_DEMO':
            time.sleep(0.25)
            self.led('blue',1)
            time.sleep(0.5)
            self.led('blue',0)
            time.sleep(0.25)
            command_processed = True
            command_clear = True

        elif ctype_enum == 'TURN_ON_SAMA5_LED_DEMO':
            self.led('blue',1)
            command_processed = True
            command_clear = True
            
        elif ctype_enum == 'TURN_OFF_SAMA5_LED_DEMO':
            self.led('blue',0)
            command_processed = True
            command_clear = True

        elif ctype_enum == 'UPDATE_SAMA5_FIRMWARE_LED_DEMO':
            command_processed = False
            command_clear = False

        # handle unknown commands
        else:
            command_processed = False
            command_clear = False

        # clear the command
        if command_clear:
            self.led('green',1)
            self.iotery.setCommandInstanceAsExecuted(commandInstanceUuid=command_uuid,
                                                     data={'timestamp':int(time.time())})
            self.led('green',0)

        # log (only relevant commands)
        if command_processed or command_clear:
            self.log('COMMAND: {} processed={} cleared={}'.format(ctype_enum,command_processed,command_clear),'info')

    def led(self,color=None,value=None):
        '''LED handler (sugar).
        Checks mpio_ok status.
        color: "red"|"green"|"blue", else ALL
        value: None|False|0|"OFF"|"off" = OFF, else ON'''

        if mpio_ok:

            colors = ('red','green','blue')
            brightness = 0

            if color in colors:
                colors = (color,)

            if value and value not in ('off','OFF'):
                brightness = 255

            for color in colors:
                LED(color).brightness = brightness

#-----------------------------------------------------------------------
# self start
#-----------------------------------------------------------------------

if __name__ in ('__main__','main','boot'):
    run()

#-----------------------------------------------------------------------
# end
#-----------------------------------------------------------------------
