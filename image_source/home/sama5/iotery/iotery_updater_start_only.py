# reference

import os,sys
import docker

#docker run -d --rm --privileged --memory-swap 128M --memory 32M --name iotery_demo1 -v /home/sama5/shared:
#/home/sama5/shared registry.gitlab.com/general-iot-platform/iotery-setup-sama5-led python3 /home/sama5/iote
#ry/iotery_demo1_robust.py

# DEMO1 RESPONSE: 26 {'status': 'success', 'activeNotificationInstances': {'device': [], 'network': []},
# 'unexecutedCommands': {'device': [{'uuid': 'd4b489f9-fbff-11e9-b548-d283610663ec',
# 'deviceUuid': '402e8c04-fa81-11e9-b548-d283610663ec', 'commandTypeUuid': '13bb3e1f-fa81-11e9-b548-d283610663ec',
# 'batchReferenceUuid': None, 'name': None, 'timestamp': 1572541181, 'isUnexecuted': True,
# 'setExecutedTimestamp': None, 'commandFields': {'FIRMWARE_VERSION_FOR_UPDATE_SAMA5_LED_DEMO': 'latest'},
# '_data': [{'enum': 'FIRMWARE_VERSION_FOR_UPDATE_SAMA5_LED_DEMO', 'value': 'latest',
# 'commandFieldUuid': '13d990d7-fa81-11e9-b548-d283610663ec'}], 'created': '2019-10-31T16:59:41.000Z',
# 'commandTypeEnum': 'UPDATE_SAMA5_FIRMWARE_LED_DEMO'}], 'network': []}}

client = docker.from_env()

client.containers.run('registry.gitlab.com/iotery-sama5/images/demo1:stable',
                      detach=True,
                      auto_remove=True,
                      privileged=True,
                      memswap_limit='128M',
                      mem_limit='32M',
                      name='iotery_demo1',
                      volumes={'/home/sama5/shared':{'bind':'/home/sama5/shared','mode':'rw'}},
                      command='python3 /home/sama5/iotery/iotery_demo1_robust.py'
                      )

print('CONTAINERS:',client.containers.list())

