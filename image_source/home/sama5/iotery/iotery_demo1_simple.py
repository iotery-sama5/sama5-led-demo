#!/usr/bin/python3

# Iotery example file 'iotery_demo1_simple.py'.
# This is a simple example with no error handling (fails on error).
# It is intended for illustration (lots of notes) and debugging.

# If the mpio module is available, the green LED indicates upload,
# and the blue LED indicates command execution. The red is not used. 

# basic imports
import os,sys,time,json

# external packages (available from PyPi using pip)
# REQUIRED: iotery-embedded-python-sdk     - python3 -m pip install iotery-embedded-python-sdk
# OPTIONAL: microchip mpio for LED control - python3 -m pip install mpio

# Iotery SDK
from iotery_embedded_python_sdk import Iotery
iotery = Iotery()

# Microchip MPIO
try:
    from mpio import LED
    mpio_ok = True
except:
    print('MPIO NOT AVAILABLE')
    mpio_ok = False

# clear (turn off) LEDs
if mpio_ok:
    for color in ('red','green','blue'):
        LED(color).brightness = 0

# shared paths
thisdir   = os.path.dirname(os.path.abspath(__file__))
parentdir = os.path.dirname(thisdir)
sharedir  = os.path.join(parentdir,'shared')
configdir = os.path.join(sharedir,'config')

# shared config
sys.path.append(configdir)
import shared_config
config = shared_config.SharedConfig()

# read shared data file to get the Iotery credentials for the device
# you can change the credentials using the built-in web server
shared_data = config.read()

# get credentials
teamid       = shared_data.get('teamUuid'    ,None)
devicename   = shared_data.get('deviceName'  ,None)
deviceserial = shared_data.get('deviceSerial',None)
devicekey    = shared_data.get('deviceKey'   ,None)
devicesecret = shared_data.get('deviceSecret',None)
print('TEAM UUID:',teamid)
print('DEVICE NAME:',devicename)
print('DEVICE SERIAL:',deviceserial)
print('DEVICE KEY:',devicekey)
print('DEVICE SECRET:',devicesecret)

# get auth token from Iotery
upload_data = {'key':devicekey,
               'serial':deviceserial,
               'secret':devicesecret,
               'teamUuid':teamid}
print([upload_data])
print([json.dumps(upload_data)])
device_data = iotery.getDeviceTokenBasic(data=upload_data)
token = device_data['token']
token_expiration = device_data['expiration']

# set auth token in SDK (used on every call to Iotery)
iotery.set_token(token)

# get Iotery data about device
my_data = iotery.getMe()
device_name = my_data['name']
device_uuid = my_data['uuid']
device_type_uuid = my_data['deviceType']['uuid']

# drop unused data
del device_data,my_data

# display collected data
print('IOTERY TOKEN:',token)
print('TOKEN EXPIRATION:',token_expiration,time.strftime("%a %d %b %Y %H:%M:%S %Z",time.localtime(token_expiration)))
print('DEVICE NAME:',device_name)
print('DEVICE UUID:',device_uuid)
print('DEVICE TYPE UUID:',device_type_uuid)

# loop (post data, receive commands, execute commands)
while 1:

    # turn green LED on during connect
    if mpio_ok:
        LED('green').brightness = 255

    # make upload data
    if mpio_ok and LED('blue').brightness:
        state = 1
    else:
        state = 0
    data = {'cpu_temp':0, # add CPU temp function here 
            'local_time':time.strftime("%a %d %b %Y %H:%M:%S %Z",time.localtime()),
            'LED State':state}

    # make upload packet
    packet = {'timestamp':int(time.time()),
              'deviceUuid':device_uuid,
              'deviceTypeUuid':device_type_uuid,
              'data':data}

    # load/receive data
    response = iotery.postData(deviceUuid=device_uuid,data={"packets":[packet]})

    # set green LED off after connect
    if mpio_ok:
        LED('green').brightness = 0

    # print response
    print('RESPONSE:',response)

    # handle any unexecuted commands
    for command in response['unexecutedCommands']['device']:

        command_uuid  = command['uuid']
        ctype_uuid    = command['commandTypeUuid']
        ctype_enum    = command['commandTypeEnum']
        command_clear = False # option to control clearing command after execution

        # each command has a unique 'commandTypeUuid' that identifies it
        # get the 'commandTypeUuid' from Iotery when you set up the command
        # then you can use it to perform a very specific function
        if ctype_uuid == 'xxxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx':
            print('there is no way this uuid will match')
            command_clear = True

        # each command also has a 'commandTypeEnum' that identifies it
        # get the 'commandTypeEnum' from Iotery when you set up the command
        # then you can use it to perform a more general function

        elif ctype_enum == 'BLINK_SAMA5_LED_DEMO':
            print('BLINK_SAMA5_LED_DEMO:',end=' ')
            time.sleep(0.25)
            if mpio_ok:
                LED('blue').brightness = 255
            print('ON',end=' ')
            time.sleep(0.5)
            if mpio_ok:
                LED('blue').brightness = 0
            print('OFF')
            time.sleep(0.25)
            command_clear = True

        elif ctype_enum == 'TURN_ON_SAMA5_LED_DEMO':
            print('TURN_ON_SAMA5_LED_DEMO:',end=' ')
            if mpio_ok:
                LED('blue').brightness = 255
            print('ON')
            command_clear = True
            
        elif ctype_enum == 'TURN_OFF_SAMA5_LED_DEMO':
            print('TURN_OFF_SAMA5_LED_DEMO:',end=' ')
            if mpio_ok:
                LED('blue').brightness = 0
            print('OFF')
            command_clear = True

        elif ctype_enum == 'UPDATE_SAMA5_FIRMWARE_LED_DEMO':
            command_clear = False

        # this will handle unknown commands
        else:
            print('UNKNOWN COMMAND:',ctype_enum,ctype_uuid)
            command_clear = False

        # clear the command
        if command_clear:
            print('CLEARING COMMAND:',ctype_enum,ctype_uuid,end=' ')
            iotery.setCommandInstanceAsExecuted(commandInstanceUuid=command_uuid,
                                                data={'timestamp':int(time.time())})
            print('DONE')

    # wait 2 seconds before next loop
    time.sleep(2)

# end







