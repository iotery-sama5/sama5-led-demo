#!/usr/bin/python3

#-----------------------------------------------------------------------
# notes
#-----------------------------------------------------------------------

# Iotery updater file 'iotery_updater_v1.py'.

# This a more robust example of using Python3 + Iotery Embedded SDK to connect to the iotery.com API.
# This will attempt to re-establish a connection with Iotery as long as only IoteryException is raised.
# This will also monitor the shared data file "shared/iotery_setup.json" for credential changes.

# Use the log file to know what is happening. The server will display this, also /bin/sharedlog (tail script).

# short delay before doing anything
# allows device to become stable

import time
print('Updater start delay: 20 seconds.')
time.sleep(20)

#-----------------------------------------------------------------------
# imports
#-----------------------------------------------------------------------

# internal imports
import os,sys,time,json,traceback

# external packages (available from PyPi using pip)
# REQUIRED: iotery-embedded-python-sdk ~ python3 -m pip install iotery-embedded-python-sdk
# REQUIRED: python docker SDK ~ python3 -m pip install docker

# Iotery SDK
from iotery_embedded_python_sdk import Iotery,IoteryException

# docker
import docker

# shared paths
thisdir   = os.path.dirname(os.path.abspath(__file__))
parentdir = os.path.dirname(thisdir)
sharedir  = os.path.join(parentdir,'shared')
logdir    = os.path.join(sharedir,'logs')
configdir = os.path.join(sharedir,'config')

# shared logger setup
sys.path.append(logdir)
import shared_logger
logger = shared_logger.SharedLogger()
logger.pname = 'UPDT'
logger.show_logs = True

# shared device config
sys.path.append(configdir)
import shared_config
config = shared_config.SharedConfig()

#-----------------------------------------------------------------------
# example run function
#-----------------------------------------------------------------------

def run():

    while 1:
        
        try:
            updater = UPDATER()
            updater.run_forever()

        except:
            config.update({'status_updater':'crashed'})
            logger.log('Major ERROR. Will attempt restart in 5 minutes.')
            time.sleep(60*5)

#-----------------------------------------------------------------------
# main class
#-----------------------------------------------------------------------

class UPDATER:
    '''Iotery Embedded Python SDK Updater
    '''

    # static user variables
    loop_every = 60 # seconds
    on_error_restart = 60 # seconds
    show_response = True

    # function variables
    shared_data_file_mtime = 0

    # docker container variables
    docker_image = 'registry.gitlab.com/iotery-sama5/images/demo1'
    docker_version = 'stable'
    docker_container = None

    def __init__(self):
        '''Init function. No input.
        Set variables directly if needed.
        '''

        # shared logger
        self.log = logger.log
        self.log('INIT','info')

        # Iotery SDK
        self.iotery = Iotery()

        # docker
        self.client = docker.from_env()

    def run_forever(self):
        '''Main loop. Robust.
        Attempts re-connect on iotery error.
        Attempts to restart container.
        '''

        # catch script errors
        # log error then re-raise same error
        try:

            # main loop
            while 1:

                # init status
                config.update({'status_updater':'not connected'})

                # read config/shared data
                shared_data = config.read()
                self.shared_data_file_mtime = config.mtime

                # no data
                if not shared_data:
                    config.update({'status_updater':'config data missing'})
                    self.log('No shared data. Restart in {} seconds.'.format(self.on_error_restart),'debug')
                    time.sleep(self.on_error_restart)
                    continue

                # parse data
                teamid       = shared_data.get('teamUuid'    ,None)
                devicename   = shared_data.get('deviceName'  ,None)
                deviceserial = shared_data.get('deviceSerial',None)
                devicekey    = shared_data.get('deviceKey'   ,None)
                devicesecret = shared_data.get('deviceSecret',None)

                # missing data
                missing = [x for x in (teamid,deviceserial,devicekey,devicesecret) if not x]
                if missing:
                    config.update({'status_updater':'config data missing'})
                    self.log('Missing config items. Restart in {} seconds.'.format(self.on_error_restart),'debug')
                    time.sleep(self.on_error_restart)
                    continue

                # get auth token from Iotery
                try:

                    # required upload data
                    upload_data = {"key":devicekey,
                                   "serial":deviceserial,
                                   "secret":devicesecret,
                                   "teamUuid":teamid}

                    # make request (will raise error if auth fails)
                    device_data = self.iotery.getDeviceTokenBasic(data=upload_data)

                    # returned token data
                    token = device_data['token']
                    token_expiration = device_data['expiration']

                    # set auth token in SDK (used on every call to Iotery)
                    self.iotery.set_token(token)

                    # get Iotery data about device
                    my_data = self.iotery.getMe()
                    device_name = my_data['name']
                    device_uuid = my_data['uuid']
                    device_type_uuid = my_data['deviceType']['uuid']

                    # drop unused data
                    del device_data,my_data

                    # update shared data status
                    config.update({'status_updater':'connected'})
                    self.log('Connected to Iotery.','info')

                # Iotery error, continue will cause a re-connect
                except IoteryException:
                    config.update({'status_updater':'auth response errer caused break'})
                    message  = 'Iotery auth response error.\n'
                    message += traceback.format_exc().strip()
                    message += '\nRestart in {} seconds.'.format(self.on_error_restart)
                    self.log(message,'error')
                    time.sleep(self.on_error_restart)
                    continue

                # non-Iotery errors will cause crash          

                # display collected data
                if self.show_response:
                    print('UPDT IOTERY TOKEN:',token)
                    print('UPDT TOKEN EXPIRATION:',token_expiration,time.strftime("%a %d %b %Y %H:%M:%S %Z",time.localtime(token_expiration)))
                    print('UPDT DEVICE NAME:',device_name)
                    print('UPDT DEVICE UUID:',device_uuid)
                    print('UPDT DEVICE TYPE UUID:',device_type_uuid)

                # auth success, go into secondary loop

                # secondary loop
                nextloop = 0
                loop2count = 0
                loop2_success = False
                container_starts = 0
                while 1:
                    loop2count += 1

                    # wait period
                    while time.time() < nextloop:
                        time.sleep(self.loop_every/100)
                    nextloop = time.time() + self.loop_every

                    # check token timeout
                    if time.time() > token_expiration - 600:
                        self.log('Iotery token expired.','info')
                        break

                    # check config
                    if self.shared_data_file_mtime != config.mtime:
                        shared_data = config.read()
                        self.shared_data_file_mtime = config.mtime
                        # need to re-connect to iotery
                        if (shared_data.get('teamUuid',None) and shared_data.get('teamUuid',None) != teamid) or\
                           (shared_data.get('deviceSerial',None) and shared_data.get('deviceSerial',None) != deviceserial) or\
                           (shared_data.get('deviceKey',None) and shared_data.get('deviceKey',None) != devicekey) or\
                           (shared_data.get('deviceSecret',None) and shared_data.get('deviceSecret',None) != devicesecret):
                            self.log('Config data has changed.','info')
                            break

                    # start container (no container exists)
                    if (not self.docker_container) or (self.docker_container not in self.client.containers.list()):
                        version = shared_data.get('version',self.docker_version)
                        container_starts += 1
                        self.start_one_image(version,container_starts)

                    # update container status
                    else:
                        self.docker_container = self.client.containers.get(self.docker_container.id)

                        # restart if status is bad
                        # status can be (created, restarting, running, removing, paused, exited, dead)
                        if self.docker_container.status in ('paused','exited','dead'):
                            container_starts += 1
                            self.start_one_image(self.docker_version,container_starts)

                        # config container status reset
                        elif self.docker_container.status != (shared_data.get('status_container','')+' x x').split()[1]:
                            version = shared_data.get('version',self.docker_version)
                            iname,iversion,idigest = self.get_image_data(self.docker_container.image,version)
                            config.update({'status_container':'{} {}'.format(iversion,self.docker_container.status)})
                            self.log('Container status: {} {}'.format(iversion,self.docker_container.status),'info')
                
                    # make upload data
                    upload_data = self.upload_data_builder()
                    packet = {'timestamp':int(time.time()),
                              'deviceUuid':device_uuid,
                              'deviceTypeUuid':device_type_uuid,
                              'data':upload_data}

                    # catch errors
                    try:

                        # upload data
                        response = self.iotery.postData(deviceUuid=device_uuid,data={"packets":[packet]})

                        # show response
                        if self.show_response:
                            print('UPDT RESPONSE:',loop2count,response)

                        # handle commands
                        for command in response['unexecutedCommands']['device']:
                            self.command_handler(command)

                        # loop2 success
                        loop2_success = True

                        # reset config status
                        if shared_data.get('status_updater',None) != 'connected':
                            config.update({'status_updater':'connected'})
                            self.log('Connection to Iotery okay.','info')

                    # iotery error, break will cause a re-connect
                    except IoteryException:
                        config.update({'status_updater':'auth response error caused break'})
                        message  = 'Iotery auth response error.\n'
                        message += traceback.format_exc().strip()
                        message += '\nRestart in {} seconds.'.format(self.on_error_restart)
                        self.log(message,'error')
                        time.sleep(self.on_error_restart)
                        break

                    # other exceptions
                    except Exception as e:
                        message  = 'Unspecified class error.\n'
                        message += traceback.format_exc().strip()

                        # never had success, so allow failure/crash
                        if not loop2_success:
                            config.update({'status_updater':'unspecified class error (no restart)'})
                            message += '\nNo restart.'
                            self.log(message,'error')
                            raise e

                        # we've had success, so try again
                        else:
                            config.update({'status_updater':'unspecified class error'})
                            message += '\nRetry in {} seconds.'.format(self.on_error_restart)
                            self.log(message,'error')
                            time.sleep(self.on_error_restart)
                            continue

                    # success at this point == remain in secondary loop
                    # otherwise, break to main loop will cause re-connect

        # catch non-iotery-auth errors
        # log error then re-raise same error
        except Exception as e:
            config.update({'status_updater':'unspecified class error (no restart)'})
            message  = 'Unspecified class error.\n'
            message += traceback.format_exc().strip()
            message += '\nNo restart.'
            self.log(message,'error')
            raise e

    def upload_data_builder(self):
        '''Build data dict for upload to Iotery.
        '''

        try:

            # container data
            if self.docker_container:

                # container
                cimage = self.docker_container.image.tags
                cid = self.docker_container.id
                cname = self.docker_container.name
                cstatus = self.docker_container.status

                # image
                itags = self.docker_container.image.tags + ['']
                iname,iversion = (itags[0]+':').split(':')[:2]
                iid = self.docker_container.image.id
                idigest = (iid+':').split(':')[1]
                
            else:
                cimage,cid,cname,cstatus = None,None,None,None
                iname,iversion,idigest = None,None,None

            # make dict        
            data = {'timestamp':int(time.time()),
                    'script':'updater.py',
                    'docker':{'image':{'name':iname,
                                       'version':iversion,
                                       'digest':idigest},
                              'container':{'image':cimage,
                                           'name':cname,
                                           'id':cid,
                                           'status':cstatus}
                              }
                    }

        # catch
        except:
            data = {'timestamp':int(time.time()),
                    'script':'updater.py',
                    'error':traceback.format_exc()}

        # return dict
        return data

    def command_handler(self,command):
        '''Handle unexecuted command instances from Iotery.
        '''

        # necessary variables in command instance
        command_uuid  = command['uuid']
        ctype_uuid    = command['commandTypeUuid']
        ctype_enum    = command['commandTypeEnum']

        # flags to control clearing command after execution
        command_processed = False
        command_clear = False

        # update command 
        if ctype_enum == 'UPDATE_SAMA5_FIRMWARE_LED_DEMO':
            self.log('Image update sequence starting.','info')
            command_clear = True
            if ('commandFields' in command):
                if ('FIRMWARE_VERSION_FOR_UPDATE_SAMA5_LED_DEMO' in command['commandFields']):                    
                    version = command['commandFields']['FIRMWARE_VERSION_FOR_UPDATE_SAMA5_LED_DEMO'].strip()
                    if not version:
                        self.log('Image update version not provided.','debug')
                    else:

                        # pull new image
                        image = self.pull_image(version)
                        
                        # good pull
                        if image:

                            iname,iversion,idigest = self.get_image_data(image,version)

                            self.log('Pull image: {}'.format(iname),'info')
                            self.log('Pull image version: {} ~ {}'.format(version,iversion),'info')
                            self.log('Pull image digest: {}'.format(idigest),'info')

                            # fallback data
                            shared_data = config.read()
                            version_current = shared_data.get('version',None)
                            version_history = shared_data.get('version_history',[])
                            version_history = [x for x in [version_current]+version_history if x]

                            # start
                            self.start_one_image(iversion,start_count=0)

                            # bad start 
                            if not self.docker_container:
                                self.log('Failed to start new container. Starting fallback.','debug')
                                fallback = 0
                                fellback = False
                                for v in version_history:
                                    fallback -= 1
                                    self.start_one_image(v,start_count=fallback)
                                    if self.docker_container:
                                        self.log('Started fallback {}: {}'.format(fallback,v),'debug')
                                        fellback = True
                                        break
                                if not fellback:
                                    self.log('Could not restart a fallback container.','error')

                            # good start
                            else:
                                new_history = []
                                config.update({'version_history':version_history[:5]})
                                command_processed = True

            # process end
            if not command_processed:
                self.log('Image update sequence failed.','error')
            else:
                self.log('Image update sequence complete: {}'.format(iversion),'info')

        # unknown commands (ignore)
        else:
            command_processed = False
            command_clear = False

        # clear the command
        if command_clear:
            self.iotery.setCommandInstanceAsExecuted(commandInstanceUuid=command_uuid,data={'timestamp':int(time.time())})

        # log (only relevant commands)
        if command_processed or command_clear:
            self.log('COMMAND: {} processed={} cleared={}'.format(ctype_enum,command_processed,command_clear),'info')

    def pull_image(self,version=None):
        '''Just pull the given (or current) image.
        Don't change any object variables or config data.
        Return the image.
        '''

        # build
        if (not version) or (not version.strip()):
            version = self.docker_version
        docker_image = '{}:{}'.format(self.docker_image,version)

        # start log
        self.log('Image pull sequence starting: {}'.format(docker_image),'info')

        # make pull
        try:
            new_image = self.client.images.pull(docker_image)

            iname,iversion,idigest = self.get_image_data(new_image,version)

            if version == iversion:
                self.log('Pull status: {} okay'.format(version),'info')

            elif iversion:
                self.log('Pull status: {} okay'.format(iversion),'info')

            else:
                raise docker.errors.APIError('Image version error.')

        except docker.errors.APIError:
            new_image = None
            iversion = None
            message = traceback.format_exc().strip()
            message += '\nPull status: error'
            self.log(message,'error')

        # end log
        self.log('Image pull sequence complete: {}'.format(iversion),'info')

        # return image
        return new_image

    def start_one_image(self,version=None,start_count=0):
        '''Stop and prune all containers.
        Start new container using given version.
        Don't change any object variables or config data.
        Sets self.docker_container with new container or None.
        '''

        # build
        if (not version) or (not version.strip()):
            version = self.docker_version
        docker_image = '{}:{}'.format(self.docker_image,version)

        # start log
        self.log('Container start sequence {}: {}'.format(start_count,version),'info')

        # stop
        for container in self.client.containers.list():
            self.log('Stopping container: {} {}'.format(container.name,container.short_id),'info')
            container.stop()

        # prune
        self.client.containers.prune()
        self.log("Containers pruned.",'info')

        # start container
        try:
            self.docker_container = self.client.containers.run(docker_image,
                                                               detach=True,
                                                               auto_remove=True,
                                                               privileged=True,
                                                               memswap_limit='128M',
                                                               mem_limit='32M',
                                                               name='iotery_demo1',
                                                               volumes={'/home/sama5/shared':{'bind':'/home/sama5/shared','mode':'rw'}},
                                                               command='python3 /home/sama5/iotery/iotery_demo1_robust.py'
                                                               )

            self.log('Started container: {} {}'.format(self.docker_container.name,self.docker_container.short_id),'info')
            self.log('Container status: {}'.format(self.docker_container.status),'info')
            self.log('Container name: {}'.format(self.docker_container.name),'info')
            self.log('Container id: {}'.format(self.docker_container.id),'info')

            iname,iversion,idigest = self.get_image_data(self.docker_container.image,version)

            old_fallback = config.read().get('version_fallback',[])
            new_fallback = []
            for v in old_fallback:
                if v != iversion and v not in new_fallback:
                    new_fallback.append(v)
            new_fallback = new_fallback[:5]

            self.log('Container image: {}'.format(iname),'info')
            self.log('Container image version: {} ~ {}'.format(version,iversion),'info')
            self.log('Container image digest: {}'.format(idigest),'info')
            self.log('Container image fallback: {}'.format(new_fallback),'info')

            config.update({'status_container':'{} {}'.format(iversion,self.docker_container.status),
                           'version':iversion,
                           'version_fallback':new_fallback[:5],
                           })

        except docker.errors.APIError:
            self.docker_container = None
            iversion = None
            message = traceback.format_exc().strip()
            message += '\nContainer status: error'
            self.log(message,'error')
            config.update({'status_container':'{} {}'.format(version,'error'),
                           'version':iversion})

        # end log
        self.log('Container start sequence {} complete: {}'.format(start_count,iversion),'info')

    def get_image_data(self,image,version):

        itags = image.tags

        if '{}:{}'.format(self.docker_image,version) in itags:
            iname,iversion = self.docker_image,version

        elif itags:
            iname,iversion = (itags[0]+':None').split(':')[:2]

        iid = image.id
        idigest = (iid+':').split(':')[1]

        return iname,iversion,idigest
              
#-----------------------------------------------------------------------
# self start
#-----------------------------------------------------------------------

if __name__ in ('__main__','main','boot'):
    run()

#-----------------------------------------------------------------------
# end
#-----------------------------------------------------------------------
