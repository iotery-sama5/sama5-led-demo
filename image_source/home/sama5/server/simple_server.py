#!/usr/bin/python3

#-----------------------------------------------------------------------
# notes
#-----------------------------------------------------------------------

# relatively simple wsgi server (cgi is way too slow on the device)
# the DataHandler class is really a persistent cgi script (almost everything happens there)
# the ServerApplication has the application required for wsgi

# short delay before doing anything
# allows device to become stable

import time
print('Server start delay: 10 seconds.')
time.sleep(10)

#-----------------------------------------------------------------------
# imports
#-----------------------------------------------------------------------

import os,sys,time,re,json,traceback
import mimetypes
import urllib.request
from urllib.parse import parse_qs,urlencode

# external packages (available from PyPi using pip)
# REQUIRED: iotery-embedded-python-sdk ~ python3 -m pip install iotery_python_server_sdk
# REQUIRED: wsgiserver ~ python3 -m pip install wsgiserver
from iotery_python_server_sdk import Iotery,IoteryException
import wsgiserver

# shared paths
thisdir   = os.path.dirname(os.path.abspath(__file__))
parentdir = os.path.dirname(thisdir)
sharedir  = os.path.join(parentdir,'shared')
logdir    = os.path.join(sharedir,'logs')
configdir = os.path.join(sharedir,'config')

# logger and config
sys.path.append(logdir)
sys.path.append(configdir)
import shared_config,shared_logger
logger = shared_logger.SharedLogger()
config = shared_config.SharedConfig()
logger.pname = 'SERV'

#-----------------------------------------------------------------------
# self-run function
#-----------------------------------------------------------------------

# variables (others use defaults)
# this must be run with root privs to use ports < 1000
host = '0.0.0.0'
port = 80

def run():

    while 1:

        try:

            logger.log('Unsecure. Port {}.'.format(port),'info')

            # set up server application
            application = ServerApplication()
            application.data_handler = DataHandler().datahandler

            # start the python wsgi server
            pythonwsgiserver(host,port,application.application,False,None)

            # start the wsgiserver.py version
            # this seems to take more resources (need to test more)
            #print('Unsecure WSGI Server: Port {}'.format(port))
            #server = wsgiserver.WSGIServer(application.application,port=port)
            #server.start()

        except KeyboardInterrupt:
            break

        except:
            logger.log(traceback.format_exc(),'error')
            logger.log('ERROR in {}'.format(os.path.basename(__file__)),'debug')
            logger.log('Restart in 10 seconds.','info')
            time.sleep(10)

#-----------------------------------------------------------------------
# DataHandler.application_process (true application, called by server.application)
#-----------------------------------------------------------------------

class DataHandler:

    authtime = 0
    authperiod = 60*60 # seconds

    wwwroot = os.path.join(thisdir,'www')
    htdocs = os.path.join(wwwroot,'htdocs')
    template_file = os.path.join(htdocs,'template.html')

    def __init__(self):

        logger.log('Started.')

    def datahandler(self,ip=None,path=None,querydata={}):

        # get template
        template_html = ''
        with open(self.template_file) as f:
            template_html = f.read()
            f.close()

        # read shared data
        shared_data = config.read()

        # collect global input values
        user     = querydata.get('user',[None])[0]
        password = querydata.get('password',[None])[0]
        process  = querydata.get('process' ,['start'])[0]
        process2 = querydata.get('process2',['start'])[0]

        # evaluate auth time
        if user == shared_data.get('serveruser','sama5') and\
           password == shared_data.get('serverpassword','sama5') and\
           process == 'auth':
            self.authtime = time.time()
            if process2 and process2 != 'auth':
                process = process2
            else:
                process = 'start'
            

        # build html setup
        row1  = '<tr><td colspan="2">{}</td></tr>'
        row1r = '<tr><td class="r" colspan="2">{}</td></tr>'
        row1c = '<tr><td class="c" colspan="2">{}</td></tr>'
        row2  = '<tr><td>{}</td><td>{}</td></tr>'
        row2r = '<tr><td>{}</td><td class="r">{}</td></tr>'
        row2t = '<tr><td class="t">{}</td><td class="t">{}</td></tr>'
        row1t = '<tr><td class="t">{}</td></tr>'
        html  = ''

        # pre-process: logout
        if process == 'logout':
            self.authtime = 0
            user = None
            password = None
            process = 'start'

        # pre-process: collect data
        elif process == 'iotery_update':
            teamapikey   = querydata.get('teamApiKey'  ,[''])[0].strip()
            devicesecret = querydata.get('deviceSecret',[''])[0].strip()
            deviceserial = 'SAMA5-9C464' # demo serial = SAMA5-9C464
            try:
                iotery = Iotery(teamapikey)
                devices = iotery.getDeviceList()
                for device in devices['results']:
                    if device['serial'] == deviceserial:
                        shared_data['teamUuid']     = device['teamUuid']
                        shared_data['deviceSerial'] = deviceserial
                        shared_data['deviceName']   = device['name']
                        shared_data['deviceKey']    = device['key']
                        shared_data['deviceSecret'] = devicesecret
                        config.update(shared_data)
                        break
                del iotery,devices
            except:
                print(traceback.format_exc())
                pass
            process = 'device'

        # login
        if (not user) or (not password) or (not self.authtime)   or\
           user     != shared_data.get('serveruser','sama5')     or\
           password != shared_data.get('serverpassword','sama5') or\
           time.time() - self.authtime >= self.authperiod:
            duser,dpass = '',''
            if user == shared_data.get('serveruser','sama5'):
                duser = user
            html += '<input type="hidden" name="process" value="auth">'
            html += '<input type="hidden" name="process2" value="{}">'.format(process)
            html += '<table>'
            html += row2.format('Username:','<input type="text" name="user" maxlength="64" value="{}">'.format(duser))
            html += row2.format('Password:','<input type="password" name="password" maxlength="64" value="{}">'.format(dpass))
            html += row1r.format('<button type="submit">GO</button>')
            html += '</table>'

        # login okay
        else:

            # update authtime
            self.authtime = time.time()

            # simple credentials pass through
            html += '<input type="hidden" name="user" value="{}">'.format(user)
            html += '<input type="hidden" name="password" value="{}">'.format(password)

            # start
            if process == 'start':
                html += '<table>'
                #options  = '<button type="submit" name="process" value="iotery_docs">Iotery Docs</button>'
                options  = '<button class="big" type="submit" name="process" value="device">Device Setup</button>'
                options += '<button class="big" type="submit" name="process" value="iotery">Demo Setup</button>'
                options += '<button class="big" onclick="window.open(\'https://iotery.io/docs\',\'_blank\')">Iotery Docs</button>'
                html += row1c.format(options)
                html += '</table>'

            # iotery
            elif process == 'iotery':

                # required variables
                #https://iotery-setup-sama5-led.netlify.com/?deviceSecret=SECRET_HERE&teamApiKey=API_KEY_HERE
                teamapikey   = querydata.get('teamApiKey'  ,[''])[0].strip()
                devicesecret = querydata.get('deviceSecret',[''])[0].strip()
                skipsecret   = querydata.get('skipsecret'  ,[''])[0].strip()

                # variables given
                if skipsecret or (teamapikey and devicesecret):
                    html += '<input type="hidden" name="teamApiKey" value="{}">'.format(teamapikey)
                    html += '<input type="hidden" name="deviceSecret" value="{}">'.format(devicesecret)
                    html += '<table>'
                    options = '<button type="submit" name="process" value="iotery_update">Click Here After Setup</button>'
                    html += row1r.format(options)
                    html += '</table>'
                    #html += '<div class="iframe"><iframe src="https://iotery-setup-sama5-led.netlify.com?{}" allow="fullscreen">Your browser does not support iframes.</iframe></div>'.format(urlencode({'teamApiKey':teamapikey,'deviceSecret':devicesecret}))
                    html += '<div class="iframe"><iframe src="https://setup.iotery.io/sama5-demo?{}" allow="fullscreen">Your browser does not support iframes.</iframe></div>'.format(urlencode({'teamApiKey':teamapikey,'deviceSecret':devicesecret}))

                # get secrets
                else:
                    html += '<table>'
                    html += row1.format('<span class="small">Enter your Iotery credentials:</span>')
                    html += row2.format('Team API Key:','<input class="long" type="text" name="teamApiKey" maxlength="256">'.format(teamapikey))                    
                    html += row2.format('Device Secret:','<input class="long" type="text" name="deviceSecret" maxlength="64">'.format(devicesecret))
                    options = '<button type="submit" name="process" value="iotery">GO</button>'
                    html += row1r.format(options)
                    info = '<span class="tiny">Get your Team API Key from the <a href="https://iotery.io/system" target="_blank" title="Get API Key from Iotery.">Iotery System Page</a></span>.'
                    info += '<br><span class="tiny">Create your own Device Secret.</span>'
                    html += row1.format(info)
                    html += '</table>'

            # device
            else:

                # start table
                html += '<table>'

                # add process select row
                options  = '<button type="submit" name="process" value="device">Device Setup</button>'
                options += '<button type="submit" name="process" value="device_status">Device Status</button>'
                options += '<button type="submit" name="process" value="device_log">Device Log</button>'
                options += '<button type="submit" name="process" value="userdata">User Data</button>'
                html += row1c.format(options)

                # device setup
                if process in ('device','device_update'):

                    update_status = ''
                    
                    if process == 'device_update':

                        changed = False
                        teamid       = querydata.get('teamUuid'    ,[''])[0].strip()
                        devicename   = querydata.get('deviceName'  ,[''])[0].strip()
                        deviceserial = querydata.get('deviceSerial',[''])[0].strip()
                        devicekey    = querydata.get('deviceKey'   ,[''])[0].strip()
                        devicesecret = querydata.get('deviceSecret',[''])[0].strip()

                        for item1,item2 in ((teamid,'teamUuid'),
                                            (devicename,'deviceName'),
                                            (deviceserial,'deviceSerial'),
                                            (devicekey,'deviceKey'),
                                            (devicesecret,'deviceSecret')):
                            if item1 and item1 != shared_data.get(item2,None):
                                shared_data[item2] = item1
                                changed = True

                        if changed:
                            if config.update(shared_data):
                                update_status = ': <span class="alert">UPDATED</span>'
                            else:
                                update_status = ': <span class="alert">ERROR</span>'

                    html += row1c.format('<span class="title">Device Setup</span>{}'.format(update_status))
                    html += row2.format('Team UUID:','<input class="long" type="text" name="teamUuid" maxlength="64" value="{}">'.format(shared_data.get('teamUuid','')))
                    html += row2.format('Device Name:','<input class="long" type="text" name="deviceName" maxlength="64" value="{}">'.format(shared_data.get('deviceName','')))
                    html += row2.format('Device Serial:','<input class="long" type="text" name="deviceSerial" maxlength="64" value="{}">'.format(shared_data.get('deviceSerial','')))
                    html += row2.format('Device Key:','<input class="long" type="text" name="deviceKey" maxlength="64" value="{}">'.format(shared_data.get('deviceKey','')))
                    html += row2.format('Device Secret:','<input class="long" type="text" name="deviceSecret" maxlength="64" value="{}">'.format(shared_data.get('deviceSecret','')))
                    html += row1r.format('<button type="submit" name="process" value="device_update" >Update</button>')

                # device status
                elif process == 'device_status':
                    html += row1c.format('<span class="title">Device Status</span>')
                    html += row1.format('<span class="small">Config file status entries:</span>')
                    values = [(item,value) for item,value in shared_data.items() if item.startswith('status')]
                    if values:
                        values.sort()
                        for item,value in values:
                            html += row2t.format(item,value)
                    else:
                        html += row1c.format('<span class="small"><span class="alert">No status items.</span></span>')
                
                # device log
                elif process == 'device_log':
                    #template_html = template_html.replace('<!--extra_meta-->','<meta http-equiv="refresh" content="10;/?{}" />\n<!--extra_meta-->'.format(urlencode({'user':user,'password':password,'process':'device_log'})))
                    html += row1c.format('<span class="title">Device Log</span>')
                    html += row1.format('<span class="small">Last 32 lines of device log:</span>')
                    lines = logger.tail(32,reverse=False).split('\n')
                    wrap = ''
                    for line in lines:
                        for x in range(0,len(line),84):
                            wrap += line[x:x+84]+'<br>'
                    html += row1t.format(wrap)                    

                # user data
                elif process.startswith('userdata'):
                    update_status = ''

                    if process.endswith('_update'):

                        changed = False
                        

                        user2        = querydata.get('user2'    ,[''])[0].strip()
                        user3s       = querydata.get('user3'    ,[''])
                        password2    = querydata.get('password2',[''])[0].strip()
                        password3s   = querydata.get('password3',[''])

                        if user2 == user and len(user3s) == 2 and user3s[0] and len(user3s[0]) >= 3 and\
                           user3s[0] == user3s[1] and user3s[0] != shared_data.get('serveruser',None):
                            shared_data['serveruser'] = user3s[0]
                            user = user3s[0]
                            changed = True

                        if password2 == password and len(password3s) == 2 and password3s[0] and len(password3s[0]) >= 5 and\
                           password3s[0] == password3s[1] and password3s[0] != shared_data.get('serverpassword',None):
                            shared_data['serverpassword'] = password3s[0]
                            password = password3s[0]
                            changed = True

                        if changed:
                            if config.update(shared_data):
                                update_status = ': <span class="alert">UPDATED</span>'
                            else:
                                update_status = ': <span class="alert">ERROR</span>'

                    html += row1c.format('<span class="title">User Data</span>{}'.format(update_status))
                    html += row1c.format('<span class="small">Change Username</span>')
                    html += row2.format('Old Username:','<input type="text" name="user2" maxlength="64" value="">')
                    html += row2.format('New Username:','<input type="text" name="user3" maxlength="64" value="">')
                    html += row2.format('Retype New:','<input type="text" name="user3" maxlength="64" value="">')
                    html += row1c.format('<span class="small">Change Password</span>')
                    html += row2.format('Old Password:','<input type="password" name="password2" maxlength="64" value="">')
                    html += row2.format('New Password:','<input type="password" name="password3" maxlength="64" value="">')
                    html += row2.format('Retype New:','<input type="password" name="password3" maxlength="64" value="">')
                    html += row1r.format('<button type="submit" name="process" value="userdata_update" >Update</button>')
                    
                # finish table
                html += '</table>'

        # send
        template_html = template_html.replace('&u=p','&'+urlencode({'user':user,'password':password}))
        template_html = template_html.replace('<!--form_content-->',html+'\n<!--form_content-->')
        template_html = template_html.replace('<!--footer_content-->',time.strftime("%a %d %b %Y %H:%M:%S %Z",time.localtime())+'\n<!--footer_content-->')
        yield template_html

#-----------------------------------------------------------------------
# WSGI server with application function
#-----------------------------------------------------------------------

# replace ServerApplication.data_handler() with your function.
# Your function must take the arguments (ip,path,querydata).
# Your function must yield (iterator) blocks of un-encoded html (page data).

# the folder 'htdocs' is an open folder (free access)
# uploads go into the folder 'htdocs/uploads'

# queries with 'p=download' and 'f=filepath' cause an octet stream download

# server class
class ServerApplication:

    def __init__(self,root=None):

        # root
        self.root = os.path.join(os.path.dirname(__file__),'www')
        if root: # and os.path.isdir(root):
            self.root = root
        self.root = os.path.abspath(self.root)
        if not os.path.exists(self.root):
            os.makedirs(self.root)

        # htdocs
        self.htdocs = os.path.join(self.root,'htdocs')
        if not os.path.exists(self.htdocs):
            os.makedirs(self.htdocs)

        # upload dir
        self.uploads = os.path.join(self.root,'uploads')
        if not os.path.exists(self.uploads):
            os.makedirs(self.uploads)

        # report
        print('SERVER: root:   ',self.root)
        print('SERVER: htdocs: ',self.htdocs)
        print('SERVER: uploads:',self.uploads)

    def data_handler(self,ip=None,path=None,querydata={}):
        yield 'Default process: {}'.format(time.asctime(time.localtime()))

    def application(self,environ,start_response):

        # variables
        ip = environ.get('REMOTE_ADDR','')
        path = environ.get('PATH_INFO','').strip('/')
        querydata = qd = {}
        print('SERVER: {} {} {}'.format(time.strftime('%Y-%m-%d %H:%M:%S'),ip,path))

        # serve htdocs
        if path and path not in ('','/') and os.path.isfile(os.path.join(self.htdocs,path)):
            path2 = os.path.join(self.htdocs,path)
            content_length = os.path.getsize(path2)
            content_type,content_encoding = mimetypes.guess_type(path2)
            if not content_type:
                content_type = 'application/octet-stream'
            start_response('200 OK',[('Content-Type',content_type),('Content-Length',str(content_length))])
            openfile = open(path2,'rb')
            block_size = 10240
            for x in environ['wsgi.file_wrapper'](openfile, block_size):
                yield x
            openfile.close()

        # must process query data
        else:

            # get query data
            qd = self.get_query_data(environ,keys=None)
            print('SERVER: QUERY:',str(qd)[:96])

            # download
            if qd.get('process',[None])[0] == 'download':
                filepath = qd.get('file',[None])[0]
                if filepath and filepath not in ('','/') and os.path.isfile(os.path.join(self.htdocs,filepath)):
                    path2 = os.path.join(self.htdocs,filepath)
                    content_length = os.path.getsize(path2)
                    start_response('200 OK',[('Content-Type','application/octet-stream'),
                                             ('Content-Length',str(content_length)),
                                             ('Content-Disposition', 'attachment; filename="{}"'.format(os.path.basename(path2))),
                                             ])
                    openfile = open(path2,'rb')
                    block_size = 10240
                    for x in environ['wsgi.file_wrapper'](openfile, block_size):
                        yield x
                    openfile.close()

            # process
            else:

                # send response
                start_response('200 OK',[('Content-Type','text/html; charset=utf-8')])

                # catch
                try:

                    # send html blocks
                    for x in self.data_handler(ip,path,qd):
                        yield x.encode('utf-8')

                # catch
                except:
                    yield ('\n\nServer Error!\n\n' + traceback.format_exc()).encode('utf-8')

    def get_query_data(self,environ,keys=None):

        data = {}

        # environ variables (6)
        data['HAS_DATA']       = False # True/False there is additional data (not just environs)
        data['READ_ERROR']     = None # None or the traceback
        data['REMOTE_ADDR']    = environ.get('HTTP_X_FORWARDED_FOR',  # lighttpd forward
                                 environ.get('REMOTE_ADDR','0.0.0.0') # default
                                             ).split(',')[0].strip()
        data['PATH_INFO']      = environ.get('PATH_INFO','').strip('/') # request path 
        data['REQUEST_METHOD'] = environ.get('REQUEST_METHOD','ERROR') # request method
        data['HTTP_COOKIE']    = environ.get('HTTP_COOKIE','') # http cookie

        # get the query method
        rm = data['REQUEST_METHOD']

        # GET: data comes as unicode strings
        if rm == 'GET':
            data = parse_qs(environ.get('QUERY_STRING',''),keep_blank_values=True)

        # POST: data seems to be bytes
        elif rm == 'POST':

            # post variables
            content_type = environ.get('CONTENT_TYPE','').strip()
            try:
                content_length = int(environ.get('CONTENT_LENGTH', 0))
            except:
                content_length = 0
                data['READ_ERROR'] = traceback.format_exc()

            # multipart
            if content_type.startswith('multipart/form-data'):
                pass # no multipart allowed
                #if content_length <= 1028000:
                #    data = simple_multipart_parse(content_type,content_length,environ['wsgi.input'])
                #else:
                #    data['READ_ERROR'] = 'Multipart content greater than allowed length.'
                #    data = {}

            # simple post
            else:
                request_body = environ['wsgi.input'].read(min(content_length,1028000))
                if type(request_body) == bytes:
                    try:
                        request_body = request_body.decode('utf-8')
                    except:
                        request_body = request_body.decode('latin-1')
                data = parse_qs(request_body,keep_blank_values=True)

        # NONE: no data
        else:
            data = {}

        # select items
        if keys:
            for key in list(data.keys()):
                if key not in keys:
                    del data[key]

        # has data
        if len(data) > 6:
            data['HAS_DATA'] = True

        # done
        return data

#-----------------------------------------------------------------------
# python wsgiref.simple_server
#-----------------------------------------------------------------------

def pythonwsgiserver_no_lookup(nothing):
    return 'testing'

def pythonwsgiserver_no_logging(strformat,*args):
    return

def pythonwsgiserver(host,port,application,secure=False,keycertfile='',):

    from wsgiref.simple_server import make_server

    try:
        name = application.__self__.__class__.__name__
    except AttributeError:
        name = application.__class__.__name__
    except AttributeError:
        name = application.__name__

    # secure server
    if secure and keycertfile and os.path.isfile(keycertfile):
        import ssl
        httpd = make_server(host,port,application)
        httpd.RequestHandlerClass.address_string = pythonwsgiserver_no_lookup
        httpd.RequestHandlerClass.log_message = pythonwsgiserver_no_logging
        httpd.socket = ssl.wrap_socket(httpd.socket,certfile=keycertfile,server_side=True)
        print('SERVER: WSGI running application file "{}" SECURE on HTTPS {}:{}.'.format(name,host,port))

    # unsecure server
    else:
        httpd = make_server(host,port,application)
        httpd.RequestHandlerClass.address_string = pythonwsgiserver_no_lookup
        httpd.RequestHandlerClass.log_message = pythonwsgiserver_no_logging
        print('SERVER: WSGI running application file "{}" NOT SECURE on HTTP {}:{}.'.format(name,host,port))

    # start server
    httpd.serve_forever()

#-----------------------------------------------------------------------
# self start
#-----------------------------------------------------------------------

if __name__ in ('__main__','main','boot'):
    run()

#-----------------------------------------------------------------------
# end
#-----------------------------------------------------------------------
